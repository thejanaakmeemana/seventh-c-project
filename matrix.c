#include <stdio.h>

//This function is for creating a matrix for a given dimension
int createMatrix(int dimension,int matrix[dimension][dimension]){
  int rowCount = 0,value = 0;
  while(rowCount<dimension){
    int count = 0;
    while(count<dimension){
      printf("Enter value : ");
      scanf("%d",&value);
      matrix[rowCount][count] = value;
      count++;
    }
    rowCount++;
  }
  printf("\n");
}

//This function is for displaying a matrix
int printMatrix(int dimension,int matrix[dimension][dimension]){
  int rowCount = 0;
  while(rowCount<dimension){
    int count = 0;
    while(count<dimension){
      printf("%d \t",matrix[rowCount][count]);
      count++;
    }
    rowCount++;
    printf("\n");
  }
  printf("\n");
}

//This function is for adding 2 matrices
int addMatrix(int dimension,int firstMatrix[dimension][dimension],int secondMatrix[dimension][dimension]){
  int resultMatrix[dimension][dimension];
  int rowCount = 0;
  printf("Addition of matrices : \n");
  while(rowCount<dimension){
    int count = 0;
    while(count<dimension){
      printf("%d \t",firstMatrix[rowCount][count]+secondMatrix[rowCount][count]);
      count++;
    }
    rowCount++;
    printf("\n");
  }
  printf("\n");
}

//This function is for multiplying 2 matrices
int multiplyMatrix(int dimension,int firstMatrix[dimension][dimension],int secondMatrix[dimension][dimension]){
  int firstRowCount = 0,secondColCount = 0;
  printf("Multiplication of matrices : \n");
  while(firstRowCount<dimension){
    int secondColCount = 0;
    while(secondColCount<dimension){
      int firstColCount = 0,secondRowCount = 0,sum = 0;
      while((firstColCount<dimension)&&(secondRowCount<dimension)){
        sum = sum+(firstMatrix[firstRowCount][firstColCount]*secondMatrix[secondRowCount][secondColCount]);
        firstColCount++;
        secondRowCount++;
      }
      printf("%d \t",sum);
      secondColCount++;
    }
    printf("\n");
    firstRowCount++;
  }
}
  

int main(){
  int dimension = 0;
  printf("Enter the dimension : ");
  scanf("%d",&dimension);
  int matrix1[dimension][dimension];
  int matrix2[dimension][dimension];
  
  printf("For creating first matrix, \n");
  createMatrix(dimension,matrix1);
  printf("For creating second matrix, \n");
  createMatrix(dimension,matrix2);
  
  printf("First matrix : \n");
  printMatrix(dimension,matrix1);
  printf("Second matrix : \n");
  printMatrix(dimension,matrix2);
  
  addMatrix(dimension,matrix1,matrix2);
  multiplyMatrix(dimension,matrix1,matrix2);
}
