#include <stdio.h>
#include <string.h>

int reverseSentence();
int main(){
  reverseSentence();
}

int reverseSentence(){
  int length = 0,count = 0;
  printf("Enter maximum possible number of characters in your sentence : ");
  scanf("%d",&length);
  getchar();
  char sentence[length]; 
  printf("Enter the sentence : ");  
  fgets(sentence,length,stdin);
  count = strlen(sentence)-2;
  while(count>=0){
    printf("%c",sentence[count]);
    count--;
  }
  printf("\n");  
} 
