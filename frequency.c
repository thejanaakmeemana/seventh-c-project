#include <stdio.h>
#include <string.h>

int countFrequency();

int main(){
  countFrequency();
}

int countFrequency(){
  char letter;
  int length = 0,frequency = 0,count = 0;
  printf("Enter maximum possible number of characters in your sentence : ");
  scanf("%d",&length);
  getchar();
  char sentence[length];
  printf("Enter the sentence : ");
  fgets(sentence,length,stdin);
  printf("Enter the character : "); 
  scanf("%c",&letter);
  length = strlen(sentence)-1;
  while(count<length){
    if(sentence[count]==letter){
      frequency++;
    }
    count++;
  }
  printf("Frequency of %c = %d \n",letter,frequency);
}
